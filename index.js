function list (array, parentDom= document.body) {
    let elements = array.map((items) => {
        return `<li>${items}</li>` ;
    });
    parentDom.innerHTML = `<ul>${elements.join(" ")}</ul>`;
}
let arrayFull = ["Hello", "World", "Kiev", "Kharkiv", "Odessa", "Lviv", "1", "2", "3", "Sea", "User", 23];
list(arrayFull, document.querySelector("#list"));



